const cookieString = "; expires="+ new Date(Date.now() + (31*24*60*60*1000)).toUTCString() + ";path=/;SameSite=None; Secure";
const urlParams = new URLSearchParams(window.location.search);

function executeSearch(query) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = () => {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText)
            document.getElementById("searchResultContent").innerHTML = this.responseText;
        }
    };
    xmlhttp.open("GET", "partial/search.php?query=" + query, true);
    console.log("partial/search.php?query=" + query)
    xmlhttp.send();
}

function toggleSetting(elementId) {
    let element = document.getElementById(elementId);
    let currentlyDisplayed = element.style.display == 'none'

    closeSettings();

    element.style.display = currentlyDisplayed ? '' : 'none';
}

function closeSettings() {
    document.querySelectorAll('.secondarySettings').forEach(element => {
        element.style.display = 'none';
    });
}

function toggleMenu(elementId) {
    element = document.getElementById(elementId);
    element.style.display = element.style.display == 'none' ? '' : 'none';
}

function closeMenus() {
    document.getElementById("datesWrapper").style.display = 'none';
    document.getElementById("settingsWrapper").style.display = 'none';
}

function settingsSetTheme(theme) {
    document.cookie = "jrnlTheme=" + theme + cookieString;
    reloadThemes();
}

function settingsSetColor(color) {
    document.cookie = "jrnlColor=" + color + cookieString;
    reloadThemes();
}

function settingsSetIcon(icon) {
    document.cookie = "jrnlIcon=" + icon + cookieString;
    
    location.reload();
}

function settingsSetHeader(header) {
    document.cookie = "jrnlHeader=" + header + cookieString;
    
    location.reload();
}

function settingsSetRound(round) {
    document.cookie = "jrnlRound=" + round + cookieString;
    reloadThemes();
}

function reloadThemes() {
    xmlhttp=new XMLHttpRequest();
    xmlhttp.onreadystatechange= () => {
        document.head.innerHTML = document.head.innerHTML +  xmlhttp.responseText;
    }
    xmlhttp.open("GET", "partial/color.php", true);
    xmlhttp.send();
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

const getDateFromGetParam = () => window.location.search.substr(1).split('date=')[1]

//#endregion

if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        console.log('service worker registration in progress...');
        navigator.serviceWorker.register('service-worker.js')
        .then(() =>
            console.info('service worker registration complete.')
        , () =>
            console.error('service worker registration failure.')
        );
    })
} else {
    console.warn('service worker is not supported.');
}
