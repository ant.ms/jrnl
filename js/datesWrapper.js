//#region Month Names

const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October",  "November", "December"];

const nth = function(d) {
    if (d > 3 && d < 21) return 'th';
    switch (d % 10) {
      case 1:  return "st";
      case 2:  return "nd";
      case 3:  return "rd";
      default: return "th";
    }
  }

//#endregion



const dateWrapper = document.querySelector("#datesWrapper")

const newSettingsItem = (title, onclick, iconLeft, iconRight = null) => {
    const item = document.createElement('div')
    item.classList = "settingsItem"
    item.onclick = onclick

    const iconElement = document.createElement('i')
    iconElement.classList = `ri-${iconLeft}-line`

    const text = document.createElement('span')
    text.innerText = title

    const spacer = document.createElement('i')
    spacer.classList = iconRight ? `ri-${iconRight}-line` : "nothing"

    item.appendChild(iconElement)
    item.appendChild(text)
    item.appendChild(spacer)

    return item;
}

const splitYear = (input, isSpecial = false) => {
    const yearRegex = /(([0-9]{4})-([0-9]{2})-([0-9]{2}))/;

    // make sure this is a year
    if (!yearRegex.test(input)) return false;

    const result = input.match(yearRegex)
    return {
        year:       result[2],
        month:      result[3],
        day:        result[4],
        isSpecial:  isSpecial
    }
}

const newSecondarySettingsItem = year => {
    const setting = document.createElement('div')
    setting.classList = "secondarySettings"
    setting.style.display = "none"
    setting.id = `date${year}`
    
    return setting
}

fetch("api/getDates.php")
.then(response => response.json())
.then(raw => {
    const wrapper = document.createElement('div')
    wrapper.id = "primarySettings"
    wrapper.classList = "settingsContent"

    wrapper.appendChild(newSettingsItem("Today", () => navigateTo(navigateTo((new Date).toISOString().substring(0, 10))), "calendar-event"))
    let yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);
    wrapper.appendChild(newSettingsItem("Yesterday", () => navigateTo(yesterday.toISOString().substr(0, 10)), "calendar-event"))

    //#region Process dates and group by year
    const dates = raw.map(date => splitYear(date.date, date.specialDay));
    const uniqueYears = new Set();
    dates.forEach(date => {
        uniqueYears.add(date.year)
    });

    const years = [...uniqueYears].map(year => dates.filter(x => x.year == year))

    // foreach year
    years.forEach(year => {
        const yearElement = newSecondarySettingsItem(year[0].year)
        const yearTrigger = newSettingsItem(
            year[0].year, 
            () => toggleSetting(`date${year[0].year}`),
            "calendar-2",
            "arrow-drop-down"
        )

        // foreach day
        year.forEach(day => {
            const item = document.createElement('div')
            item.classList = "settingsItem"
            item.onclick = () => navigateTo(`${day.year}-${day.month}-${day.day}`)

            const text = document.createElement('span')
            text.innerText = `${day.day}${nth(day.day)} ${monthNames[day.month - 1]}`
            item.appendChild(text)

            if (day.isSpecial) {
                const specialIcon = document.createElement('i')
                specialIcon.classList = `ri-star-line`
                item.appendChild(specialIcon)
            }

            yearElement.appendChild(item)
        })

        wrapper.appendChild(yearTrigger)
        wrapper.appendChild(yearElement)
    })

    dateWrapper.appendChild(wrapper)
})