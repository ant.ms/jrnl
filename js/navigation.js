const navigateTo = date => {
    fetch(`api/entries.php?date=${date}`)
    .then(res => res.json())
    .then(data => {
        //#region Boxes
        const page = document.querySelector('#page');
        page.innerHTML = ''; // remove already existing cards

        let postBoxes = []
        let specialBox = data.special ? getSpecialBox(data.special) : undefined;

        data.posts?.map(post => {
            postBoxes.push(getPostBox(post))
        })

        if (postBoxes.length > 0 || specialBox) {
            specialBox && page.appendChild(specialBox)
            postBoxes?.map(postBox => page.appendChild(postBox))
        } else {
            let div = document.createElement('div')
            div.classList.add('noPostIcon')

            let icon = document.createElement('i')
            icon.classList.add(`ri-${(new Date).getHours() < 7 || (new Date).getHours() > 20 ? "moon" : "sun"}-line`)

            let subtitle = document.createElement('p')
            subtitle.innerText = "No posts here yet"

            div.appendChild(icon)
            div.appendChild(subtitle)

            page.appendChild(div)
        }

        closeMenus()
        history.pushState({}, date, `?date=${date}`)
        //#endregion

        //#region Navigation Arrows
        fetch(`api/next-prev.php?date=${date}`)
        .then(res => res.json())
        .then(data => {
            //#region left
            let leftLi = document.createElement('li')
            leftLi.id = 'navArrowLeft'
            if (data.prev) {
                leftLi.onclick = () => navigateTo(data.prev)
    
                let leftI = document.createElement('i')
                leftI.classList.add('ri-arrow-left-s-line')
    
                leftLi.appendChild(leftI)
            }
            //#endregion

            //#region right
            let rightLi = document.createElement('li')
            rightLi.id = 'navArrowRight'
            if (data.next) {
                rightLi.onclick = () => navigateTo(data.next)
    
                let rightI = document.createElement('i')
                rightI.classList.add('ri-arrow-right-s-line')
    
                rightLi.appendChild(rightI)
            }
            //#endregion

            let navArrowLeft = document.querySelector('#navArrowLeft')
            let navArrowRight = document.querySelector('#navArrowRight')
            navArrowLeft.parentNode.replaceChild(leftLi, navArrowLeft)
            navArrowRight.parentNode.replaceChild(rightLi, navArrowRight)
        })
        //#endregion

        // Input field
        document.querySelector('#dateInput').value = date
        document.querySelector('#specialDateInput').value = date
    })
}

navigateTo(getDateFromGetParam() || (new Date).toISOString().substring(0, 10))

const getPostBox = data => {
    let div = document.createElement('div')
    div.classList.add('postBox')

    let title = document.createElement('b')
    title.innerText = data.title

    let content = document.createElement('p')
    content.classList.add('notyetemoji')
    content.innerText = data.content

    div.appendChild(title)
    div.appendChild(content)
    return div;
}

const getSpecialBox = data => {
    let div = document.createElement('div')
    div.classList.add('specialBox')

    //#region image Box
    let imageBox = document.createElement('div')
    div.classList.add('specialImage')

    let imageBoxImage = document.createElement('img')
    imageBoxImage.src = data.imageurl

    imageBox.appendChild(imageBoxImage)
    //#endregion

    //#region content Box
    let contentBox = document.createElement('div')
    div.classList.add('specialContent')

    let title = document.createElement('h2')
    title.innerText = data.title

    let content = document.createElement('p')
    content.classList.add('notyetemoji')
    content.innerText = data.content

    contentBox.appendChild(title)
    contentBox.appendChild(content)
    //#endregion
    
    div.appendChild(imageBox)
    div.appendChild(contentBox)
    return div;
}