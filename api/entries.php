<?php
    include '_common.php';

    $posts = [];
        $result = $db->prepare("SELECT * FROM posts WHERE dateofcreation BETWEEN '$dateOfPosts 00:00:00' AND '$dateOfPosts 23:59:59'")->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC))
        array_push($posts, [
            "id" => $row['id'],
            "title" => $row['title'],
            "content" => $row['content']
        ]);

    $specialDay = null;
        $result = $db->prepare("SELECT * FROM specials WHERE date = '$dateOfPosts'")->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC))
        $specialDay = [
            "title" => $row['title'],
            "content" => $row['content'],
            "imageurl" => $row['imageurl']
        ];

    echo json_encode($specialDay ?
        [
            "posts" => $posts,
            "special" => $specialDay
        ]
        :
        [
            "posts" => $posts
        ]
    );
?>