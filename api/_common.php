<?php
    include '../data/config.php';
    session_start();
    
    $password = hash("sha512", $_SESSION["email"] . $_SESSION["password"]);
    if ($password != $passwdHash1 && $password != $passwdHash2) {
        echo "401 Access Denied!!";
        exit();
    }

    $db = new SQLite3('../data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $dateOfPosts = (isset($_GET['date'])) ? $_GET['date'] : date("Y-m-d");
    if ($dateOfPosts == "") $dateOfPosts = date("Y-m-d");
?>