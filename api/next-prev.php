<?php
    include '_common.php';

    function getNextDay() {
        global $db, $dateOfPosts;

        $statement = $db->prepare("SELECT dateofcreation FROM posts WHERE dateofcreation > '$dateOfPosts 23:59:59' ORDER BY dateofcreation ASC LIMIT 1");
        $result = $statement->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC))
            return substr($row["dateofcreation"], 0, 10);

        return false;
    }
    function getPrevDay() {
        global $db, $dateOfPosts;

        $statement = $db->prepare("SELECT dateofcreation FROM posts WHERE dateofcreation < '$dateOfPosts 00:00:00' ORDER BY dateofcreation DESC LIMIT 1");
        $result = $statement->execute();
        while($row = $result->fetchArray(SQLITE3_ASSOC))
            return substr($row["dateofcreation"], 0, 10);

        return false;
    }

    echo json_encode([
        "prev" => getPrevDay(),
        "next" => getNextDay()
    ]);
?>