<?php
    include '_common.php';

    $output = [];

    $statement = $db->prepare(
        "SELECT strftime('%Y-%m-%d', dateofcreation) AS date FROM posts
        UNION ALL
        SELECT date FROM specials
        GROUP BY date
        ORDER BY date DESC"
    );
    $result = $statement->execute();
    
    while($row = $result->fetchArray(SQLITE3_ASSOC)) {
        $output[] = array(
            "date" => $row['date']
        );
    }

    echo json_encode($output);
?>
