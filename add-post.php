<?php
    include 'partial/markdown.php';

    $debugging = false;

    // open database
    $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $title = strip_tags($_POST['title']);
    $content = str_replace('"', "''", strip_tags($_POST['message']));

    if ($debugging) echo "Input: <br>" . $content . "<br><br>";

    $content = applyMarkdown($content);
    $content = applyEmoji($content);

    if ($debugging) echo $content;
    
    $date = strip_tags($_POST['date']);
    $currentUser = strip_tags($_POST['user']);
    $sql = "INSERT INTO posts (title, content, createdby, dateofcreation) VALUES (\"$title\", \"$content\", $currentUser, \"$date 12:00\")";

    $db->query($sql);
    echo "<br><br>" . $sql;
    
    $db->close();

    if ($debugging) exit();

    header("Location: index.php?date=$date");
?>