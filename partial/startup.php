<?php
    // check if necessary files exist, otherwise redirect to install page
    if (!file_exists('data/posts.sqlite') || !file_exists('data/config.php'))
        header("Location: install.php");

    // get config from config file
    // (if you need to generate a new password manually, uncomment this line) echo hash("sha512", "admin" . "admin");
    include 'data/config.php';

    // set login if it needs setting
    // (this page only receives a post request, when it's tasked with logging the user in)
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $_SESSION["email"] = $_POST['email'];
        $_SESSION["password"] = $_POST['password'];
        
        header('Location: .');
    } else {
        if (isset($_GET['logout'])) {
            session_destroy(); 
            header('Location: .');
        }
    }

    $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    // date of currently selected post
    $dateOfPosts = (isset($_GET['date'])) ? $_GET['date'] : date("Y-m-d");
    if ($dateOfPosts == "")
        $dateOfPosts = date("Y-m-d");

    // special content
    $specialDays = array();

    $result = $db->prepare("SELECT date FROM specials")->execute();
    
    while($row = $result->fetchArray(SQLITE3_ASSOC) )
        array_push($specialDays, $row['date']);
?>