<div id="search" class="backgroundDarken" style="display:none">
    <div class="modal">
        <div class="modal-content">
            <h2>Search for Posts</h2>
            <div class="row">
                <input type="hidden" id="specialdate" name="specialdate" value="<?php echo $dateOfPosts ?>">
                <div class="row">
                    <div style="display:flex">
                        <input id="searchQuery" type="text" required>
                        <button onclick="executeSearch(document.getElementById('searchQuery').value)"><i class="ri-search-line" style="font-size:18px"></i></button>
                    </div>
                </div>
                <div class="row" style="margin:20px 10px">
                    <div id="searchResultContent" style="overflow-y:auto;max-height:50vh">
                        Start a search, and your content will appear here
                    </div>
                </div>
            </div>
        </div>
        <div class="modalFooter">
            <button type="submit" name="action" onclick="document.getElementById('search').style.display='none'">Cancel</button>
        </div>
    </div>
</div>