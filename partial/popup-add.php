<div id="add" class="backgroundDarken" style="display:none">
    <div class="modal">
        <div>
            <h2>Add new Post</h2>
            <div class="row">
                <form class="col s12" action="add-post.php" method="POST" id="addForm">
                    <input type="hidden" name="date" id="dateInput">
                    <input type="hidden" name="user" value="<?php echo $currentUser ?>">
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="icon_telephone"><i class="ri-heading"></i> Title</label><br>
                            <input id="icon_telephone" type="text" name="title" class="validate" required placeholder="My nice day at the beach...">
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <label for="icon_prefix2"><i class="ri-file-text-line"></i> Message</label><br>
                            <textarea id="icon_prefix2" rows="5" class="materialize-textarea" name="message" required placeholder="Today was a very gay day.."></textarea>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modalFooter">
            <button type="submit" name="action"
                onclick="document.getElementById('add').style.display='none'">Cancel</button>
            <button type="submit" name="action" onclick="document.getElementById('addForm').submit()">Submit</button>
        </div>
    </div>
</div>