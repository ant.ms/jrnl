<div id="special" class="backgroundDarken" style="display:none">
    <div class="modal" style="width: 750px;">
        <div class="modal-content">
            <h2>Add new Special Day</h2>
            <div class="row">
                <form class="col s12" action="add-special.php" method="POST" id="addSpecial" style="display:flex">
                    <input type="hidden" id="specialDateInput" name="specialdate">
                    <div style="width: 60%;">

                        <div class="row">
                            <div class="input-field col s12">
                                <label for="specialtitle"><i class="ri-heading"></i>Title</label>
                                <input id="specialtitle" type="text" name="specialtitle" class="validate" required  placeholder="My gay day at the village...">
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <label for="specialcontent"><i class="ri-file-text-<?= $iconStyle ?>"></i>Description</label>
                                <textarea id="specialcontent" rows="13" class="materialize-textarea" name="specialcontent" required placeholder="Today was a very gay day.."></textarea>
                            </div>
                        </div>

                    </div>
                    <div style="width: 40%;">
                        <!-- TODO: Fix styling -->

                        <div class="row">
                            <div class="input-field col s6">
                                <label for="specialimageurl"><i class="ri-gallery-upload-<?= $iconStyle ?>"></i> Image URL</label>
                                <input id="specialimageurl" type="url" name="specialimageurl" class="validate" placeholder="https://cdn.ant.lgbt/cutie/..." required oninput="document.getElementById('tmpspecialImage').src = this.value">
                            </div>
                        </div>
                        <div class="row">
                            <img id="tmpspecialImage" src="https://image.freepik.com/free-vector/wrinkled-paper-texture_1100-12.jpg" alt="" style="height: 200px; width: 95%; object-fit: cover;border-radius: var(--border-radius-small);margin-top: 10px;" onerror="this.src='https://image.freepik.com/free-vector/wrinkled-paper-texture_1100-12.jpg'">
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="modalFooter">
            <button type="submit" name="action"
                onclick="document.getElementById('special').style.display='none'">Cancel</button>
            <button type="submit" name="action" onclick="document.getElementById('addSpecial').submit()">Submit</button>
        </div>
    </div>
</div>