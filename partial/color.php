<style>
:root {
    --settings-popup-width: 200px;

    <?php
        function echoLightCSS() {
            echo "--text: #2e3440;
            --background: #fafafa;
            --page: #fff;
            --page-dark: #f7f7f7;
            --page-bright: #fff;

            --primary-shadow: 0 9px 18px 0 rgba(0, 0, 0, 0.17);
            --secondary-shadow: 0 6px 7px 0 rgba(200, 200, 200, 0.83);";
        }
        function echoNordCSS() {
            echo "--text: #e5e9f0;
            --background: #2e3440;
            --page: #3b4252;
            --page-dark: #313744;
            --page-bright: #3b4252;

            --primary-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.3);
            --secondary-shadow: 0 10px 20px 0 rgba(15, 15, 15, 0.3);";
        }
        function echoDarkCSS() {
            echo "--text: #e8eef2;
            --background: #181818;
            --page: #222222;
            --page-dark: #1e1e1e;
            --page-bright: #303030;
        
            --primary-shadow: 0 10px 20px 0 rgba(0, 0, 0, 0.87);
            --secondary-shadow: 0 10px 20px 0 rgba(15, 15, 15, 0.87);";
        }

        if ($_COOKIE["jrnlTheme"] == "light")
            echoLightCSS();
        else if ($_COOKIE["jrnlTheme"] == "nord")
            echoNordCSS();
        else if ($_COOKIE["jrnlTheme"] == "dark")
            echoDarkCSS();
        else {
            if (isset($_COOKIE["color_scheme"]) ? $_COOKIE["color_scheme"] : "auto" == "dark")
                echoDarkCSS();
            else
                echoLightCSS();
        }

    ?>

    <?php
        // Accent Colors
        if ($_COOKIE["jrnlColor"] == "orange")
            echo "--accent: #d08770;";
        else if ($_COOKIE["jrnlColor"] == "pink")
            echo "--accent: #b48ead;";
        else if ($_COOKIE["jrnlColor"] == "green")
            echo "--accent: #a3be8c;";
        else
            echo "--accent: #81a1c1;";
    ?>

    <?php
        // Roundness
        if ($_COOKIE["jrnlRound"] == "true")
            echo "--border-radius-large: 0;
                --border-radius-medium: 0;
                --border-radius-small: 0;";
        else
            echo "--border-radius-large: 12px;
                --border-radius-medium: 10px;
                --border-radius-small: 7px;";
    ?>

}
</style>