<div  style='margin-top: 30px'>
    <div>
        <h1>Please Login</h1>
    </div>
    <div>
        <form class='col s12' action='.' method='POST'>
            <div class='row'>
                <div class='input-field col s12'>
                    <label for='email'>Username</label></div>
                    <input id='email' name='email' type='text'>
            </div>
            <div class='row'>
                <div class='input-field col s12'>
                    <label for='password'>Password</label></div>
                    <input id='password' name='password' type='password'>
            </div>
            <div class='row'>
                <button class='btn waves-effect waves-light $selectedTheme' type='submit' name='action' style='float:right'>
                    Login
                </button>
            </div>
        </form>
    </div>
</div>