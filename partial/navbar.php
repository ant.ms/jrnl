<?php    
    $iconStyle = ($_COOKIE['jrnlIcon'] == 'fill') ? "fill" : "line";
?>
<div id='navbar'>
    <div id="navbarWrapper">

        <div id="datesWrapper" style="display: none"> <!-- TO BE FILLED BY JS --> </div>
        
        <ul>
            <li onclick="toggleMenu('datesWrapper')"><i class="ri-menu-<?= $iconStyle ?>"></i></li>
            <li id="navArrowLeft"></li>
            <li id="navArrowRight"></li>
        </ul>

        <a id="titleLogo" href="." class="hide-on-med-and-down" style="margin-left: 15px">Jrnl</a>

        <ul>
            <li>
                <a onclick="document.getElementById('search').style.display='flex'">
                    <i class="ri-search-2-<?= $iconStyle ?>"></i>
                </a>
            </li>
            <li>
                <a onclick="document.getElementById('add').style.display='flex'">
                    <i class="ri-menu-add-<?= $iconStyle ?>"></i>
                </a>
            </li>
            <li>
                <a onclick="document.getElementById('special').style.display='flex'">
                    <i class="ri-image-add-<?= $iconStyle ?>"></i>
                </a>
            </li>
            <li onclick="toggleMenu('settingsWrapper')"><i class="ri-settings-3-<?= $iconStyle ?>"></i></li>
        </ul>
    </div>

    <!-- settings -->
    
    <div id="settingsWrapper" style="display: none;">
        <div class="settingsContent" id="primarySettings">

            <div class="settingsItem" onclick="window.location = '?logout'">
                <i class="ri-logout-box-<?= $iconStyle ?>"></i>
                <span>Logout</span>
                <i class="nothing"></i>
            </div>

            <div class="settingsItem" onclick="toggleSetting('themeSetting')">
                <i class="ri-contrast-line"></i>
                <span>Theme</span>
                <i class="ri-arrow-drop-down-<?= $iconStyle ?>"></i>
            </div>

            <div class="secondarySettings" id="themeSetting" style="display: none;">
                <div class="settingsItem">
                    <span>Auto</span>
                
                    <label class="container">
                        <input type="radio" name="theme" onclick="settingsSetTheme('auto')" <?php echo ($_COOKIE['jrnlTheme'] == 'auto') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <span>Dark</span>
                
                    <label class="container">
                        <input type="radio" name="theme" onclick="settingsSetTheme('dark')" <?php echo ($_COOKIE['jrnlTheme'] == 'dark') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <span>Nord</span>
                
                    <label class="container">
                        <input type="radio" name="theme" onclick="settingsSetTheme('nord')" <?php echo ($_COOKIE['jrnlTheme'] == 'nord') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <span>Light</span>
                
                    <label class="container">
                        <input type="radio" name="theme" onclick="settingsSetTheme('light')" <?php echo ($_COOKIE['jrnlTheme'] == 'light') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>


            <div class="settingsItem" onclick="toggleSetting('colorSetting')">
                <i class="ri-brush-<?= $iconStyle ?>"></i>
                <span>Color</span>
                <i class="ri-arrow-drop-down-<?= $iconStyle ?>"></i>
            </div>

            <div class="secondarySettings" id="colorSetting" style="display: none;">
                <div class="settingsItem">
                    <div>
                        <span style="color:#81a1c1 !important">Blue</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="colors" onclick="settingsSetColor('blue')" <?= ($_COOKIE['jrnlColor'] == 'blue') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <div>
                        <span style="color:#d08770 !important">Orange</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="colors" onclick="settingsSetColor('orange')" <?= ($_COOKIE['jrnlColor'] == 'orange') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <div>
                        <span style="color:#b48ead !important">Pink</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="colors" onclick="settingsSetColor('pink')" <?= ($_COOKIE['jrnlColor'] == 'pink') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <div>
                        <span style="color:#a3be8c !important">Green</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="colors" onclick="settingsSetColor('green')" <?= ($_COOKIE['jrnlColor'] == 'green') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>


            <div class="settingsItem" onclick="toggleSetting('iconSetting')">
                <i class="ri-remixicon-<?= $iconStyle ?>"></i>
                <span>Icons</span>
                <i class="ri-arrow-drop-down-<?= $iconStyle ?>"></i>
            </div>

            <div class="secondarySettings" id="iconSetting" style="display: none;">
                <div class="settingsItem">
                    <div>
                        <span>Line</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="color" onclick="settingsSetIcon('line')" <?= ($_COOKIE['jrnlIcon'] == 'line') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <div>
                        <span>Fill</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="color" onclick="settingsSetIcon('fill')" <?= ($_COOKIE['jrnlIcon'] == 'fill') ? 'checked' : '' ?>>
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>


            <div class="settingsItem" onclick="toggleSetting('layoutSettings')">
                <i class="ri-layout-<?= $iconStyle ?>"></i>
                <span>Layout</span>
                <i class="ri-arrow-drop-down-<?= $iconStyle ?>"></i>
            </div>

            <div class="secondarySettings" id="layoutSettings" style="display: none;">
                <div class="settingsItem">
                    <div>
                        <span>Square</span>
                    </div>
                    
                    <label class="switch">
                        <input type="checkbox" onclick="settingsSetRound(this.checked)" <?php echo ($_COOKIE['jrnlRound'] == 'true') ? 'checked="checked"' : '' ;?>>
                        <span class="slider round"></span>
                    </label>
                </div>

            </div>


            <!-- TODO: Make work in JS -->
            <div class="settingsItem" onclick="toggleSetting('headingSetting')">
                <i class="ri-heading"></i>
                <span>Header</span>
                <i class="ri-arrow-drop-down-<?= $iconStyle ?>"></i>
            </div>

            <div class="secondarySettings" id="headingSetting" style="display: none;">
                <div class="settingsItem">
                    <div>
                        <span>Bold</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="color" onclick="settingsSetHeader('bold')" <?php echo ($_COOKIE['jrnlHeader'] == 'bold') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

                <div class="settingsItem">
                    <div>
                        <span>Header</span>
                    </div>
                    
                    <label class="container">
                        <input type="radio" name="color" onclick="settingsSetHeader('header')" <?php echo ($_COOKIE['jrnlHeader'] == 'header') ? 'checked="checked"' : '' ;?>>
                        <span class="checkmark"></span>
                    </label>
                </div>

            </div>
        </div>
    </div>

</div>