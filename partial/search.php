<?php
    // open database
    $db = new SQLite3('../data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $searchQuery = $_GET['query'];

    $result = $db->prepare("SELECT * FROM posts WHERE (`title` LIKE '%".$searchQuery."%') OR (`content` LIKE '%".$searchQuery."%') LIMIT 5;")->execute();
    
    while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
        // convert new lines to linebreaks
        $row = str_replace("\n", "<br>", $row);

        echo '<div class="postBox" style="width:90%">';

        // Format Title correctly
        if ($_COOKIE['jrnlHeader'] == "header")
            echo "<h2>" . $row['title'] . "</h2>";
        else
            echo "<b>" . $row['title'] . "</b><br>";

        // echo content
        echo "<p class='notyetemoji'>" . $row['content'] . "</p>"; 

        echo "</div>";
    }
?>