<?php
    include 'partial/markdown.php';
    include 'data/config.php';

    // open database
    $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    $title = strip_tags($_POST['specialtitle']);
    $content = str_replace('"', "''", strip_tags($_POST['specialcontent']));

    $content = applyMarkdown($content);
    $content = applyEmoji($content);

    $date = strip_tags($_POST['specialdate']);
    $specialimageurl = strip_tags($_POST['specialimageurl']);
    $db->query("INSERT INTO specials (date, title, content, imageurl) VALUES (\"$date\",\"$title\",\"$content\",\"$specialimageurl\")");

    echo "INSERT INTO specials (date, title, content, imageurl) VALUES (\"$date\",\"$title\",\"$content\",\"$specialimageurl\")";

    $db->close();

    header("Location: .?date=$date");
?>