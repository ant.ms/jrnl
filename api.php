<?php
    // get config from config file
    // (if you need to generate a new password manually, uncomment this line) echo hash("sha512", "admin" . "admin");
    include 'data/config.php';

    // open database
    $db = new SQLite3('data/posts.sqlite', SQLITE3_OPEN_CREATE | SQLITE3_OPEN_READWRITE);

    function getSubPathInFile() {
        $withoutBeginning = explode("api.php/", $_SERVER['REQUEST_URI'])[1];
        return explode("?", $withoutBeginning)[0];
    }

    if ($_GET["key"] != $apiKey1 && $_GET["key"] != $apiKey2) {
        echo "ERROR: Access Denied!!! (wrong API key)";
        exit();
    }

    // /api.php/dates

    if (getSubPathInFile() == "dates") {

        $output = [];

        $statement = $db->prepare("SELECT strftime('%Y-%m-%d', dateofcreation) AS dates FROM posts GROUP BY dates ORDER BY dates DESC");
        $result = $statement->execute();
        
        while($row = $result->fetchArray(SQLITE3_ASSOC)) {
            $output[] = array(
                "date"=>$row['dates'],
                "specialDay"=>false
            );
        }

        echo json_encode($output);

    } else if (getSubPathInFile() == "post") {

        $output = "[";

        $dateOfPosts = $_GET["date"];
        $result = $db->prepare("SELECT * FROM posts WHERE dateofcreation BETWEEN '$dateOfPosts 00:00:00' AND '$dateOfPosts 23:59:59'")->execute();
    
        while($row = $result->fetchArray(SQLITE3_ASSOC) ) {
            $id = $row['id'];
            $title = $row['title'];
            $content = $row['content'];
            $output = $output . "{\"id\": \"$id \",\"title\": \"$title \",\"content\": \"$content\"},";
        }

        // echo substr_replace($output, "", -1) . "]";
        echo substr_replace(trim(preg_replace('/\s\s+/', ' ', ($output))),"", -1) . "]";

    }else {
        echo "ERROR: No query specified";
    }
?>