<?php session_start(); ?>

<!DOCTYPE html>
<html>
    <head>
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"> -->
        <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;500;600&display=swap"> -->
        <link href="https://fonts.googleapis.com/css2?family=PT+Sans:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="main.css">
        <link rel="manifest" href="manifest.json">

        <title>Jrnl</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="img/favicon.png" type="image/png">

        <script src="js/script.js" defer></script>
        <script src="js/navigation.js" defer></script>
        <script src="js/datesWrapper.js" defer></script>

        <?php include 'partial/color.php'; ?>
    </head>

    <?php include 'partial/startup.php'; ?>

    <body>

        <div id="wrapper">

            <!-- Main Page -->
            <div id="mainWrapper">
                <!-- Navigation Bar -->
                <?php
                    include 'partial/navbar.php';
                ?>
                <div id="page">
                    
                    <!--  Login -->
                    <?php
                        $password = hash("sha512", $_SESSION["email"] . $_SESSION["password"]);
                        if ($password != $passwdHash1 && $password != $passwdHash2) {
                            include 'partial/login.php';
                            exit();
                        } else {
                            if ($password == $passwdHash1)
                                $currentUser = 1;
                            else if ($password == $passwdHash2)
                                $currentUser = 2;
                            else {
                                $currentUser = 0;
                            }
                        }
                    ?>

                    <!-- TO BE FILLED BY JS -->
                </div>
            </div>
        </div>

    </body>

    <?php include 'partial/popup-add.php'; ?>
    <?php include 'partial/popup-add-special.php'; ?>
    <?php include 'partial/popup-search.php'; ?>
</html>

<script src="https://cdn.jsdelivr.net/npm/js-cookie/dist/js.cookie.min.js"></script>
<script>
    // code to set the `color_scheme` cookie
    let $color_scheme = Cookies.get("color_scheme");
    function get_color_scheme() {
        return (window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches) ? "dark" : "light";
    }
    function update_color_scheme() {
        Cookies.set("color_scheme", get_color_scheme());
    }
    // read & compare cookie `color-scheme`
    if ((typeof $color_scheme === "undefined") || (get_color_scheme() != $color_scheme))
        update_color_scheme();
    // detect changes and change the cookie
    if (window.matchMedia)
        window.matchMedia("(prefers-color-scheme: dark)").addListener( update_color_scheme );
</script>