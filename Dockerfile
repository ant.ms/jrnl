FROM php:apache
COPY . /var/www/html/
RUN echo "output_buffering = 1" >> /usr/local/etc/php/conf.d/php.ini
RUN chmod -R 777 /var/www/html/data